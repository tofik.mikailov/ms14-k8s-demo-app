package az.ingress.ms14k8sdemoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms14K8sDemoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms14K8sDemoAppApplication.class, args);
    }

}
